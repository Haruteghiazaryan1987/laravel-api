<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Channel;
use App\Models\Playlist;
use App\Models\Video;
use Illuminate\Support\Collection;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class PlaylistVideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
/*        $playlists = Playlist::all();

        $playlistVideos = $playlists->flatMap(fn (Playlist $playlist)=>$this->playlistVideos($playlist,$this->randomVideosFrom($playlist->channel)));*/
//        DB::table('playlist_video')->insert($playlistVideos->all());

        Playlist::with('channel.videos')
            ->each(fn (Playlist $playlist)=>
            $playlist->videos()->saveMany($this->randomVideosFrom($playlist->channel)));
    }

//    private function playlistVideos(Playlist $playlist,Collection $videos):Collection
//    {
//        return $videos->map(fn (Video $video)=>[
//            'playlist_id'=>$playlist->id,
//            'video_id'=>$video->id,
//            'channel_id'=>$playlist->channel->id,
//        ]);
//    }

    private function randomVideosFrom(Channel $channel)
    {
/*        if ($channel->videos->isEmpty()){
            return collect();
        }
        return $channel->videos->random(mt_rand(1,count($channel->videos)));*/

        return $channel->videos->whenEmpty(
            fn()=>collect(),
            fn(Collection $videos)=>$videos->random(mt_rand(1,count($channel->videos))),
        );
    }


}
