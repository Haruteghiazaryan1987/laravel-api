<?php

namespace App\Enums;

use Carbon\Carbon;

enum Period: string
{
    case Year = 'year';
    case Month = 'month';
    case Week = 'week';
    case Day = 'day';
    case Hour = 'hour';

    public function date(): Carbon
    {
        return match ($this){
            Period::Year => now()->startOfYear(),
            Period::Month=> now()->startOfMonth(),
            Period::Week => now()->startOfWeek(),
            Period::Day => now()->startOfDay(),
            Period::Hour => now()->startOfHour()
        };
    }
}


