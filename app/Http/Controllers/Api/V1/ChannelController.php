<?php

namespace App\Http\Controllers\Api\V1;

use App\Enums\Period;
use App\Http\Controllers\Controller;
use App\Models\Channel;
use App\Models\Model;
use Illuminate\Database\Eloquent\Collection;

class ChannelController extends Controller
{
    public function index()
    {
        return Channel::query()->withRelationship(request('with'))
            ->search(request('query'))
            ->orderBy(request('sort', 'name'), request('order', 'asc'))
            ->simplePaginate(request('limit'));
    }

    public function show(Channel $channel): Model
    {
        return $channel->loadRelationships(request('with'));
    }
}
