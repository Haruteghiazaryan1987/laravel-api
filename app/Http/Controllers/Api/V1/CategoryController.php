<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Model;
use Illuminate\Database\Eloquent\Collection;

class CategoryController extends Controller
{
    public function index()
    {
        //$cat=Category::query()->find(1)->videos;   //Many to many
        return Category::query()->withRelationship(request('with'))
            ->search(request('query'))
            ->orderBy(request('sort', 'name'), request('order', 'asc'))
            ->simplePaginate(request('limit'));
    }

    public function show(Category $category): Model
    {
        //        $category_by_id=Category::query()->find($category);
        return $category->loadRelationships(request('with'));
    }
}
