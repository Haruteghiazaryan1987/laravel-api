<?php

namespace App\Http\Controllers\Api\V1;

use App\Enums\Period;
use App\Http\Controllers\Controller;
use App\Models\Model;
use App\Models\Video;

class VideoController extends Controller
{
    public function index()
    {
//        $vid=Video::query()->find(1)->categories;   // many to many
//        return Video::with('channel', 'categories')->get();
//        return dd(Video::where('created_at', '>',now()->startOfMonth())->get()->count());

        return Video::query()->withRelationship(request('with'))
            ->fromPeriod(Period::tryFrom(request('period')))
            ->search(request('query'))
            ->orderBy(request('sort', 'created_at'), request('order', 'desc'))
            ->simplePaginate(request('limit'));
//        return [
//            'Year' => Video::where('created_at', '>', now()->startOfYear())->get()->count(),
//            'Month' => Video::where('created_at', '>', now()->startOfMonth())->get()->count(),
//            'Week' => Video::where('created_at', '>', now()->startOfWeek())->get()->count(),
//            'Day' => Video::where('created_at', '>', now()->startOfDay())->get()->count(),
//        ];
    }

    public function show(Video $video): Model
    {
        return $video->loadRelationships(request('with'));
    }
}
