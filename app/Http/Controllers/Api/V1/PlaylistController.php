<?php

namespace App\Http\Controllers\Api\V1;

use App\Enums\Period;
use App\Http\Controllers\Controller;
use App\Models\Channel;
use App\Models\Model;
use App\Models\Playlist;
use Illuminate\Database\Eloquent\Collection;

class PlaylistController extends Controller
{
    public function index()
    {
        return Playlist::query()->withRelationship(request('with'))
            ->search(request('query'))
            ->orderBy(request('sort', 'name'), request('order', 'asc'))
            ->simplePaginate(request('limit'));
    }

    public function show(Playlist $playlist): Model
    {
        return $playlist->loadRelationships(request('with'));
    }
}
