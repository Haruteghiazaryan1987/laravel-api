<?php

namespace App\Http\Controllers\Api\V1;

use App\Enums\Period;
use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Model;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Throwable;

class CommentController extends Controller
{
    public function index()
    {
        return Comment::withRelationship(request('with'))
            ->fromPeriod(Period::tryFrom(request('period')))
            ->search(request('query'))
            ->orderBy(request('sort', 'created_at'), request('order', 'desc'))
            ->simplePaginate(request('limit'));
    }

    public function show(Comment $comment): Model
    {
        return $comment->loadRelationships(request('with'));
    }

    public function store(Request $request)
    {
        $attributes = $request->validate([
            'text' => 'required|string',
            'parent_id' => 'exists:comments,id',
            'video_id' => 'required_without:parent_id|exists:videos,id',
        ]);

        $attributes['user_id']=$request->user()->id;
        return Comment::create($attributes);

    }

    /**
     * @throws AuthorizationException
     * @throws Throwable
     */
    public function update(Comment $comment, Request $request): void
    {
        $this->checkPermission($comment);
        $attributes = $request->validate([
            'text' => 'required|string',
        ]);

        $comment->fill($attributes)->save();
    }

    /**
     * @throws Throwable
     */
    public function destroy(Comment $comment): void
    {
        $this->checkPermission($comment);

        $comment->delete();
    }

    /**
     * @throws Throwable
     */
    private function checkPermission(Comment $comment): void
    {
        Gate::allowIf(fn (User $user)=>$comment->isOwnedBy($user));
    }

}
