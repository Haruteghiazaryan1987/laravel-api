<?php

namespace App\Models;

use App\Enums\Period;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Comment extends Model
{
    use HasFactory;

    protected static array $relationships = ['user', 'video'];

    protected $guarded = [];
    protected static function booted(): void
    {
        static::saving(function (Comment $comment) {
            $comment->user_id = $comment->user_id ?: auth()->id();

            if ($comment->parent_id) {
                $comment->video_id = Comment::find($comment->parent_id)->video_id;
            }
        });
    }
    public function parent(): BelongsTo
    {
        return $this->belongsTo(static::class);
    }
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    public function video(): BelongsTo
    {
        return $this->belongsTo(Video::class);
    }
    public function scopeFromPeriod($query, ?Period $period)
    {
        return $period ? $query->where('created_at', '>=', $period->date()) : $query;
    }

    public function scopeSearch($query, ?string $text)
    {
        return $query->where('text', 'like', "%$text%");
    }

    public function isOwnedBy(User $user): bool
    {
        return $this->user_id === $user->id;
    }

    public function replies(): HasMany
    {
        return $this->hasMany(static::class,'parent_id');
    }
}
